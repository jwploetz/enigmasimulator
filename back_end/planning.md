**order of algorithms**  
1. input text and settings  
2. convert to capital and remove spaces and punctuation  
3. plugboard  
4. rotors right to left  
5. reflector  
6. rotors left to right  
7. plugboard  
8. output  

**rotors**  
'I' : 'EKMFLGDQVZNTOWYHXUSPAIBRCJ', #wheel I  
'II' : 'AJDKSIRUXBLHWTMCQGZNPYFVOE', #wheel II  
'III' : 'BDFHJLCPRTXVZNYEIWGAKMUSQO', #wheel III  
'IV' : 'ESOVPZJAYQUIRHXLNFTGKDCMWB', #wheel IV  
'V' : 'VZBRGITYUPSDNHLXAWMJQOFECK', #wheel V  
'VI' : 'JPGVOUMFYQBENHZRDKASXLICTW', #wheel VI  
'VII' : 'NZJHGRCXMYSWBOUFAIVLPEKQDT', #wheel VII  
'VIII' : 'FKQHTLXOCBJSPDZRAMEWNIUYGV' # wheel VIII  

**rotor notches**  
'I' : 'Q', #wheel I  
  'II' : 'E', #wheel II  
  'III' : 'V', #wheel III  
  'IV' : 'J', #wheel IV  
  'V' : 'Z', #wheel V  
  'VI' : 'ZM', #wheel VI  
  'VII' : 'ZM', #wheel VII  
  'VIII' : 'ZM' # wheel VIII  

**reflectors**
{'A':'Y', 'Y':'A', 'B':'R', 'R':'B', 'C':'U', 'U':'C', 'D':'H', 'H':'D', 'E':'Q', 'Q':'E', 'F':'S', 'S':'F', 'G':'L', 'L':'G', 'I':'P', 'P':'I', 'J':'X', 'X':'J', 'K':'N', 'N':'K', 'M':'O', 'O':'M', 'T':'Z', 'Z':'T', 'V':'W', 'W':'V'} #B  
{'A':'F', 'F':'A', 'B':'V', 'V':'B', 'C':'P', 'P':'C', 'D':'J', 'J':'D', 'E':'I', 'I':'E', 'G':'O', 'O':'G', 'H':'Y', 'Y':'H', 'K':'R', 'R':'K', 'L':'Z', 'Z':'L', 'M':'X', 'X':'M', 'N':'W', 'W':'N', 'Q':'T', 'T':'Q', 'S':'U', 'U':'S'} #C  
