alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

#inputs
rotors_in_a = 'II'
rotors_in_b = 'IV'
rotors_in_c = 'III'
plug_pairs = 'AB CD EF' #input("plugboard pairs?  >>> ").upper()
ring_settings = 'AAA' #input("ring settings   >>> ").upper()
start_pos = 'AAA' #input("starting positions   >>> ").upper()
reflector_in = 'UKW-B'

#rotor and reflector settings
rotors_dict = {
    'I' : 'EKMFLGDQVZNTOWYHXUSPAIBRCJ', #wheel I
    'II' : 'AJDKSIRUXBLHWTMCQGZNPYFVOE', #wheel II
    'III' : 'BDFHJLCPRTXVZNYEIWGAKMUSQO', #wheel III
    'IV' : 'ESOVPZJAYQUIRHXLNFTGKDCMWB', #wheel IV
    'V' : 'VZBRGITYUPSDNHLXAWMJQOFECK', #wheel V
    'VI' : 'JPGVOUMFYQBENHZRDKASXLICTW', #wheel VI
    'VII' : 'NZJHGRCXMYSWBOUFAIVLPEKQDT', #wheel VII
    'VIII' : 'FKQHTLXOCBJSPDZRAMEWNIUYGV' # wheel VIII
}

notch_dict = {
    'I' : 'Q', #wheel I
    'II' : 'E', #wheel II
    'III' : 'V', #wheel III
    'IV' : 'J', #wheel IV
    'V' : 'Z', #wheel V
    'VI' : 'ZM', #wheel VI
    'VII' : 'ZM', #wheel VII
    'VIII' : 'ZM' # wheel VIII
}

reflector_b = {'A':'Y', 'Y':'A', 'B':'R', 'R':'B', 'C':'U', 'U':'C', 'D':'H', 'H':'D', 'E':'Q', 'Q':'E', 'F':'S', 'S':'F', 'G':'L', 'L':'G', 'I':'P', 'P':'I', 'J':'X', 'X':'J', 'K':'N', 'N':'K', 'M':'O', 'O':'M', 'T':'Z', 'Z':'T', 'V':'W', 'W':'V'}
reflector_c = {'A':'F', 'F':'A', 'B':'V', 'V':'B', 'C':'P', 'P':'C', 'D':'J', 'J':'D', 'E':'I', 'I':'E', 'G':'O', 'O':'G', 'H':'Y', 'Y':'H', 'K':'R', 'R':'K', 'L':'Z', 'Z':'L', 'M':'X', 'X':'M', 'N':'W', 'W':'N', 'Q':'T', 'T':'Q', 'S':'U', 'U':'S'}


def caesar_shift(str, amount):
	output = ""

	for i in range(0,len(str)):
		code = ord(str[i])
		if ((code >= 65) and (code <= 90)):
			c = chr(((code - 65 + amount) % 26) + 65)
		output = output + c

	return output


if reflector_in == 'UKW-B':
  reflector = reflector_b
else:
  reflector = reflector_c


rotor_a_notch = notch_dict[rotors_in_a]
rotor_b_notch = notch_dict[rotors_in_b]
rotor_c_notch = notch_dict[rotors_in_c]

rotor_a_letter = start_pos[0]
rotor_b_letter = start_pos[1]
rotor_c_letter = start_pos[2]

rotor_a_setting = ring_settings[0]
rotor_b_setting = ring_settings[1]
rotor_c_setting = ring_settings[2]

offset_a_setting = alphabet.index(rotor_a_setting)
offset_b_setting = alphabet.index(rotor_b_setting)
offset_c_setting = alphabet.index(rotor_c_setting)

rotor_a = caesar_shift(rotors_dict[rotors_in_a], offset_a_setting)
rotor_b = caesar_shift(rotors_dict[rotors_in_b], offset_b_setting)
rotor_c = caesar_shift(rotors_dict[rotors_in_c], offset_c_setting)

#shifts wiring if offset is present
if offset_a_setting > 0:
    rotor_a = rotor_a[26 - offset_b_setting:] + rotor_a[0:26 - offset_a_setting]
if offset_b_setting > 0:
    rotor_b = rotor_b[26 - offset_b_setting:] + rotor_b[0:26 - offset_b_setting]
if offset_c_setting > 0:
    rotor_c = rotor_c[26 - offset_c_setting:] + rotor_c[0:26 - offset_c_setting]

plugboard_connections = plug_pairs.upper().split(" ")
plugboard_dict = {}
for pair in plugboard_connections:
    if len(pair) == 2:
        plugboard_dict[pair[0]] = pair[1]
        plugboard_dict[pair[1]] = pair[0]

def plugboard_encrypt(char):
    if char in plugboard_dict.keys():
        if plugboard_dict[char]!="":
            char = plugboard_dict[char]
    return char

def rotor_step():
    global rotor_a_letter, rotor_b_letter, rotor_c_letter
    rotor_trigger = False

    if rotor_c_letter == rotor_c_notch:
        rotor_trigger = True
    rotor_c_letter = alphabet[(alphabet.index(rotor_c_letter) + 1) % 26]

    if rotor_trigger:
        rotor_trigger = False
        if rotor_b_letter == rotor_b_notch:
            rotor_trigger = True
        rotor_b_letter = alphabet[(alphabet.index(rotor_b_letter) + 1) % 26]

        if (rotor_trigger):
            rotor_trigger = False
            rotor_a_letter = alphabet[(alphabet.index(rotor_a_letter) + 1) % 26]

    else:
        if rotor_b_letter == rotor_b_notch:
            rotor_b_letter = alphabet[(alphabet.index(rotor_b_letter) + 1) % 26]
            rotor_a_letter = alphabet[(alphabet.index(rotor_a_letter) + 1) % 26]

def rotor_encrypt(char, rotor, offset, direction):
    pos = alphabet.index(char)
    if direction:
        let = rotor[(pos + offset) % 26]
        pos = alphabet.index(let)
    else:
        let = alphabet[(pos + offset) % 26]
        pos = rotor.index(let)
    char = alphabet[(pos - offset + 26) % 26]
    return char

def encrypt(plaintext):
    ciphertext = ''

    for i in plaintext:
        rotor_step()

        offset_a = alphabet.index(rotor_a_setting)
        offset_b = alphabet.index(rotor_b_setting)
        offset_c = alphabet.index(rotor_c_setting)

        i = plugboard_encrypt(i)

        i = rotor_encrypt(i, rotor_c, offset_c, True)
        i = rotor_encrypt(i, rotor_b, offset_b, True)
        i = rotor_encrypt(i, rotor_a, offset_a, True)

        if i in reflector.keys():
          if reflector[i]!="":
            i = reflector[i]

        i = rotor_encrypt(i, rotor_a, offset_a, False)
        i = rotor_encrypt(i, rotor_b, offset_b, False)
        i = rotor_encrypt(i, rotor_c, offset_c, False)

        i = plugboard_encrypt(i)

        ciphertext += i

    return ciphertext

texttocipher = 'RMXXT' #input("input   >>> " ).upper()
texttocipher = texttocipher.replace(" ","")
try:
    ciphertext = encrypt(texttocipher)
    print(ciphertext)
except:
    print('Sorry, an error occured. We are still working out the bugs, hopefully it will work soon. \nFor now you can try it with different settings.')
