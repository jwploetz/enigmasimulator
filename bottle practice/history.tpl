<!DOCTYPE html>
<html>
<head>
  <link rel="icon" href="images/favicon.png" type="image/png">
  <meta charset="utf-8">
  <style>
  html, body {
    margin: auto;
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;
    background-image: url("images/background.jpg"),url("images/background.jpg");
    background-repeat: repeat-y;
    background-position: left, right;
    background-size: 20%;
    flex-direction: column;
    background-color: AntiqueWhite;
  }
  footer, header {
    font-family: Arial, Helvetica, sans-serif;
    overflow: hidden;
    background-color: #333;
    width: 100%;
  }
  footer {
    position: fixed;
    width: 100%;
    bottom: 0;
    font-size: 17px;
    color: white;
  }
  .topnav a {
    float: left;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    font-size: 17px;
  }
  .topnav a:hover {
    background-color: #ddd;
    color: black;
  }
  .topnav a.active {
    background-color: blue;
    color: white;
  }
  .alignleft {
  	float: left;
    margin-left: 10px;
  }
  .alignright {
  	float: right;
    margin-right: 10px;
  }
  .divwithtext {
    margin: auto;
    height: 100%;
    width: 60%;
    background-color: AntiqueWhite;
    float: center;
  }
  </style>
</head>
<body>
  <header>
    <div class="topnav">
      <a href="/">Home</a>
      <a class="active" href="history.html">History</a>
      <a href="design.html">Schematic details</a>
      <a href="funnypic.html">Funny Picture</a>
    </div>
  </header>
  <div class="divwithtext">
    <h1>The History of the Enigma Cipher</h1>
    <br>
    <p>The Enigma machine was invented by a German engineer Arthur Scherbius shortly after WW1. There were many different models of the machine produced, but each of them operated on the same basic principle, you would type a letter on the portion of the machine that resembled a typewriter, and the enciphered letter would light up. With the combination of a plugboard to initially transpose letter, interchangeable rotor wheels, ring setting, and reflector settings, there are 103 sextillion possible settings to choose from, and as such it was believed to be unbreakable at the time of its invention. The code was broken as early as 1932 in Poland, and with the prospect of war, they decided to share this information in 1939 with the British. A team composed of Dilly Nox, Tony Kendrick, later joined by Peter Twinn, Alan Turing and Gordon Welchman. Together they set a station at Bletchley Park and routinely broke wartime Enigma messages for the remainder of the war. </p>
    <br>
    <p>How was the code broken in Britain? Namely Alan Turing. He was a brilliant mathematician who alongside another code breaker, Gordon Welchman, created a device known as the Bombe. This device not only helped him crack the Enigma code, but many other code breakers through the war. An important piece of information is that the Polish were the predecessors to the Bombe, having invented smaller “bombes” to crack specific German messages. It is because of this contribution by the Polish that it is estimated the war ended and astounding two years earlier than it would have. It seems that the German army never concluded that their cipher had been broken, they were just curious as to how some information was getting leaked. They also had some success in breaking allied ciphers, but not on the same scale as breaking the Enigma cipher. The reason the Germans did not find out that the cipher was broken was because the allied forces still allowed some attacks to be carried out if they weren’t that big, to ward off suspicions.</p>
    <br>
    <p>It is unknown how many Enigma machines were produced, but estimates place it between 40 and 50 thousand. There are still about 300 surviving today in various museums and displays. During the war, some Enigma machines were stolen, but this might be less helpful than you think. Just because they had an Enigma machine did not mean that they could decode the messages, because there were still 103 sextillion possible combinations. Code breakers were still needed.</p>
    <br>
    <p>What is considered to be the biggest flaw in the Enigma machine is the fact that it cannot ever cipher letters to themselves, as an example, M ciphering to M. While this may not seem like a very big flaw, it is one of the key principles in the designing of the Bombe machine. This was because it meant codebreakers could look for specific words or phrases that they guessed would be in the message and use that to determine the cipher settings. This was not too difficult, as the transmissions often started with a weather report and then ended with “Heil Hitler”. </p>
    <br>
    <p>Overall the Enigma cipher is one of, if not the strongest ciphers ever produced. It would have even theoretically been unbreakable, had the operating procedures been upheld better by the German army.</p>
  </div>
  <footer>
    <p class="alignleft">ITP 100 Final Project - Enigma Simulator</p>
    <p class="alignright">By Ploetz and JFK</p>
  <footer>
</body>
</html>
