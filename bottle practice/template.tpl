<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Hello World In A Bottle</title>
<style type="text/css">
body {
    margin: 25px;
    padding: 25px;
}
main {
    border: 1px dotted #555;
}
h1 {
    padding: 15px;
    text-align: center;
    font-size: 3em;
    font-weight: bold;
}
p {
    text-align: center;
    font-size: 1.5em;
}
a, a:visited {
    color: #663;
    text-decoration: none;
}
footer {
    margin-top: 5px;
    text-align: center;
}
</style>
</head>
<body>

<main>
<h1>{{message}}</h1>

<p>Add <code>/[message]</code> (example: <code>/New York</code>) to change
the message.</p>
</main>

<footer>
<a href="http://validator.w3.org/check/referer">
<strong> HTML </strong> Valid! </a> |
<a href="http://jigsaw.w3.org/css-validator/check/referer?profile=css3">
<strong> CSS </strong> Valid! </a>
</footer>

</body>
</html>
