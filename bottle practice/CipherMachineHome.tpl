<!DOCTYPE html>
<html>
<head>
  <link rel="icon" href="images/favicon.png" type="image/png">
  <meta charset="utf-8">
  <script type="text/javascript"
      src="https://cdn.jsdelivr.net/npm/brython@3.9.0/brython.min.js">
  </script>
  <style>
  html, body {
    margin: auto;
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;
    background-image: url("images/background.jpg"),url("images/background.jpg");
    background-repeat: repeat-y;
    background-position: left, right;
    background-size: 20%;
    flex-direction: column;
    background-color: AntiqueWhite;
  }
  footer, header {
    font-family: Arial, Helvetica, sans-serif;
    overflow: hidden;
    background-color: #333;
    width: 100%;
  }
  footer {
    position: fixed;
    width: 100%;
    bottom: 0;
    font-size: 17px;
    color: white;
  }
  .topnav a {
    float: left;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    font-size: 17px;
  }
  .topnav a:hover {
    background-color: #ddd;
    color: black;
  }
  .topnav a.active {
    background-color: blue;
    color: white;
  }
  .alignleft {
  	float: left;
    margin-left: 10px;
  }
  .alignright {
  	float: right;
    margin-right: 10px;
  }
  .divwithtext {
    margin: auto;
    height: 100%;
    width: 60%;
    background-color: AntiqueWhite;
    float: center;
  }
  </style>
</head>
<body>
  <header>
    <div class="topnav">
      <a class="active" href="CipherMachineHome.html">Home</a>
      <a href="/history">History</a>
    </div>
  </header>
  <div class="divwithtext">
  <h1>Enigma Cipher</h1>
  <p>This is the an Enigma machine simulator. Give your input in the box below to cipher it.</p>
  <br>
  <p>Input Text: </p>
  <input type="text" id="input-box" placeholder="add text here...">
  <br>
  <br>
  <table style="margin-left:auto;margin-right:auto;border-spacing: 5px;">
    <tr>
      <td>Rotor 1</td>
      <td>Rotor 2</td>
      <td>Rotor 3</td>
    </tr>
    <tr>
      <td>
        <select id="rotor1">
          <option value="I"selected>I</option>
          <option value="II">II</option>
          <option value="III">III</option>
          <option value="IV">IV</option>
          <option value="V">V</option>
        </select>
      </td>
      <td>
        <select id="rotor2">
          <option value="I">I</option>
          <option value="II"selected>II</option>
          <option value="III">III</option>
          <option value="IV">IV</option>
          <option value="V">V</option>
        </select>
      </td>
      <td>
        <select id="rotor3">
          <option value="I">I</option>
          <option value="II">II</option>
          <option value="III"selected>III</option>
          <option value="IV">IV</option>
          <option value="V">V</option>
        </select>
      </td>
    </tr>
  </table>
  <p>Reflector<p>
    <select id="reflector_in">
      <option value="UKW-B">UKW-B</option>
      <option value="UKW-C">UKW-C</option>
    </select>
  <br>
  <table style="margin-left:auto;margin-right:auto;border-spacing: 5px;">
    <tr>
      <td><p>Plugboard Pairs: </p></td>
      <td><input type="text" id="plug-pair-input" value="AB CD EF"/></td>
    </tr>
    <tr>
      <td><p>Ring Settings: </p></td>
      <td><input type="text" id="ring-setting-input" value="AAA"/></td>
    </tr>
    <tr>
      <td><p>Rotor Starting Position: </p></td>
      <td><input type="text" id="rotor-setting-input" value="AAA"/></td>
  </table>
  <br>
  <button onclick="brython()">Cipher</button>
  <br>
  <br>
  <h4> Output: </h4>
  <p id="output">-</p>
  <br>
  <br>
  <br>
  </div>
  <footer>
    <p class="alignleft">ITP 100 Final Project - Enigma Simulator</p>
    <p class="alignright">By Ploetz and JFK</p>
  <footer>
  <script type="text/python">
    from browser import document
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

    rotors_in_a = str(document.getElementById("rotor1").value)
    rotors_in_b = str(document.getElementById("rotor2").value)
    rotors_in_c = str(document.getElementById("rotor3").value)
    plug_pairs = str(document.getElementById("plug-pair-input").value).upper()
    ring_settings = str(document.getElementById("ring-setting-input").value).upper()
    start_pos = str(document.getElementById("rotor-setting-input").value).upper()
    reflector_in = str(document.getElementById("reflector_in").value)


    def shift(str, amount):
    	output = ""

    	for i in range(0,len(str)):
    		code = ord(str[i])
    		if ((code >= 65) and (code <= 90)):
    			c = chr(((code - 65 + amount) % 26) + 65)
    		output = output + c

    	return output


    rotors_dict = {
        'I' : 'EKMFLGDQVZNTOWYHXUSPAIBRCJ', #wheel I
        'II' : 'AJDKSIRUXBLHWTMCQGZNPYFVOE', #wheel II
        'III' : 'BDFHJLCPRTXVZNYEIWGAKMUSQO', #wheel III
        'IV' : 'ESOVPZJAYQUIRHXLNFTGKDCMWB', #wheel IV
        'V' : 'VZBRGITYUPSDNHLXAWMJQOFECK', #wheel V
        'VI' : 'JPGVOUMFYQBENHZRDKASXLICTW', #wheel VI
        'VII' : 'NZJHGRCXMYSWBOUFAIVLPEKQDT', #wheel VII
        'VIII' : 'FKQHTLXOCBJSPDZRAMEWNIUYGV' # wheel VIII
    }

    notch_dict = {
        'I' : 'Q', #wheel I
        'II' : 'E', #wheel II
        'III' : 'V', #wheel III
        'IV' : 'J', #wheel IV
        'V' : 'Z', #wheel V
        'VI' : 'ZM', #wheel VI
        'VII' : 'ZM', #wheel VII
        'VIII' : 'ZM' # wheel VIII
    }

    reflector_b = {'A':'Y', 'Y':'A', 'B':'R', 'R':'B', 'C':'U', 'U':'C', 'D':'H', 'H':'D', 'E':'Q', 'Q':'E', 'F':'S', 'S':'F', 'G':'L', 'L':'G', 'I':'P', 'P':'I', 'J':'X', 'X':'J', 'K':'N', 'N':'K', 'M':'O', 'O':'M', 'T':'Z', 'Z':'T', 'V':'W', 'W':'V'}
    reflector_c = {'A':'F', 'F':'A', 'B':'V', 'V':'B', 'C':'P', 'P':'C', 'D':'J', 'J':'D', 'E':'I', 'I':'E', 'G':'O', 'O':'G', 'H':'Y', 'Y':'H', 'K':'R', 'R':'K', 'L':'Z', 'Z':'L', 'M':'X', 'X':'M', 'N':'W', 'W':'N', 'Q':'T', 'T':'Q', 'S':'U', 'U':'S'}

    if reflector_in == 'UKW-B':
      reflector = reflector_b
    else:
      reflector = reflector_c

    #assigning all rotor settings
    rotor_a_notch = notch_dict[rotors_in_a]
    rotor_b_notch = notch_dict[rotors_in_b]
    rotor_c_notch = notch_dict[rotors_in_c]

    rotor_a_letter = start_pos[0]
    rotor_b_letter = start_pos[1]
    rotor_c_letter = start_pos[2]

    rotor_a_setting = ring_settings[0]
    rotor_b_setting = ring_settings[1]
    rotor_c_setting = ring_settings[2]

    offset_a_setting = alphabet.index(rotor_a_setting)
    offset_b_setting = alphabet.index(rotor_b_setting)
    offset_c_setting = alphabet.index(rotor_c_setting)

    rotor_a = shift(rotors_dict[rotors_in_a], offset_a_setting)
    rotor_b = shift(rotors_dict[rotors_in_b], offset_b_setting)
    rotor_c = shift(rotors_dict[rotors_in_c], offset_c_setting)


    #shifts "wiring" if offset is present
    if offset_a_setting > 0:
        rotor_a = rotor_a[26 - offset_a_setting:] + rotor_a[0:26 - offset_a_setting]
    if offset_b_setting > 0:
        rotor_b = rotor_b[26 - offset_b_setting:] + rotor_b[0:26 - offset_b_setting]
    if offset_c_setting > 0:
        rotor_c = rotor_c[26 - offset_c_setting:] + rotor_c[0:26 - offset_c_setting]

    #converts plugboard settings into a dictionary
    plugboard_connections = plug_pairs.upper().split(" ")
    plugboard_dict = {}
    for pair in plugboard_connections:
        if len(pair) == 2:
            plugboard_dict[pair[0]] = pair[1]
            plugboard_dict[pair[1]] = pair[0]


    def plugboard_encrypt(char):
      if char in plugboard_dict.keys():
        if plugboard_dict[char] != "":
            char = plugboard_dict[char]
      return char


    def rotor_step():
      global rotor_a_letter, rotor_b_letter, rotor_c_letter
      rotor_trigger = False

      if rotor_c_letter == rotor_c_notch:
        rotor_trigger = True
      rotor_c_letter = alphabet[(alphabet.index(rotor_c_letter) + 1) % 26]

      if rotor_trigger:
        rotor_trigger = False
        if rotor_b_letter == rotor_b_notch:
          rotor_trigger = True
        rotor_b_letter = alphabet[(alphabet.index(rotor_b_letter) + 1) % 26]

        if (rotor_trigger):
          rotor_trigger = False
          rotor_a_letter = alphabet[(alphabet.index(rotor_a_letter) + 1) % 26]

      else:
        if rotor_b_letter == rotor_b_notch:
          rotor_b_letter = alphabet[(alphabet.index(rotor_b_letter) + 1) % 26]
          rotor_a_letter = alphabet[(alphabet.index(rotor_a_letter) + 1) % 26]


    def rotor_encrypt(char, rotor, offset, direction):
      pos = alphabet.index(char)
      if direction:
        let = rotor[(pos + offset) % 26]
        pos = alphabet.index(let)
      else:
        let = alphabet[(pos + offset) % 26]
        pos = rotor.index(let)
      char = alphabet[(pos - offset + 26) % 26]
      return char


    def encrypt(plaintext):
      ciphertext = ''

      for i in plaintext:
        if i in alphabet:
          offset_a = alphabet.index(rotor_a_letter)
          offset_b = alphabet.index(rotor_b_letter)
          offset_c = alphabet.index(rotor_c_letter)

          rotor_step()

          i = plugboard_encrypt(i)

          i = rotor_encrypt(i, rotor_c, offset_c, True)
          i = rotor_encrypt(i, rotor_b, offset_b, True)
          i = rotor_encrypt(i, rotor_a, offset_a, True)

          i = reflector[i]

          i = rotor_encrypt(i, rotor_a, offset_a, False)
          i = rotor_encrypt(i, rotor_b, offset_b, False)
          i = rotor_encrypt(i, rotor_c, offset_c, False)

          i = plugboard_encrypt(i)

          ciphertext += i

        else:
          ciphertext += i

      return ciphertext

    texttocipher = str(document.getElementById("input-box").value).upper()

#    try:
#      ciphertext = encrypt(texttocipher)
#      document.getElementById("output").innerHTML = ciphertext
#    except:
#      ciphertext = 'Sorry, an error occured. We are still working out the bugs, hopefully it will work soon. For now you can try it with different settings.'
#      document.getElementById("output").innerHTML = ciphertext
    ciphertext = encrypt(texttocipher)
    document.getElementById("output").innerHTML = ciphertext

  </script>
  <a href="/history">other page</a>
</body>
</html>
