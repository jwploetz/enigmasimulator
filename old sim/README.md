# ITP 100 Final Project - Enigma Simulator

### Download


This project is a simulator of the Enigma Cipher, which was used in WWII by the German army. It is a very powerful cipher which used multiple forms of substitution along with constantly changing cipher settings. There are 103 sextillion different combinations of settings that can be created. This machine's cipher was broken by the British army around 1941 with the help of the Polish army. The intelligence gained from the breaking of the Enigma cipher brought the war to a close an estimated two years early. This is one of the most powerful cipher machines ever produced, if not the most powerful. Read more about it in the History page on our website.

## Description  
This is a Jack and Jack collaboration project, Jack will do the back end server side using bottle, and Jack will be reworking the original program to be object oriented and more historically accurate.

## History


## Images  
