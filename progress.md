![servers](https://www.racksolutions.com/news//app/uploads/AdobeStock_90603827-scaled.jpeg)

NON programming progress updates:

1. gained access to server to use, going to work on how to use it

2. made a decision to use some brython as well because mixing things is coolio

3. found someone to show me how to use a non-localhost server, working on it this weekend

4. Helpful source [here](https://www.hostinger.com/tutorials/website/how-to-upload-your-website)

5. Video that i am not sure we'll need [here](https://www.youtube.com/watch?v=NQP89ish9t8&ab_channel=freeCodeCamp.org)

6. ssh into server today, working on running something on it. Going is a little slow but this is all new to me so.

7. Actually we made big progress today, we were able to access our site on another computer which is big.

8. I don't know how o show you what I am doing on the server, but I fixed it up so we have a link that can take you to our website and you can go to other pages. We basically just have some code revision left to do on the cipher.
